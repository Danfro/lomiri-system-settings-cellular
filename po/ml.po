# Malayalam translation for lomiri-system-settings-cellular
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the lomiri-system-settings-cellular package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-cellular\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-29 13:32+0000\n"
"PO-Revision-Date: 2023-01-04 19:45+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Malayalam <https://hosted.weblate.org/projects/lomiri/"
"lomiri-system-settings-cellular/ml/>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.15.1-dev\n"
"X-Launchpad-Export-Date: 2015-07-16 05:41+0000\n"

#: ../plugins/cellular/PageApnEditor.qml:71
#, fuzzy
msgid "Edit"
msgstr "Edit"

#: ../plugins/cellular/PageApnEditor.qml:71
msgid "New APN"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:132
msgid "Used for:"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:136
#: ../plugins/cellular/PageChooseApn.qml:107
msgid "Internet and MMS"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:137
#: ../plugins/cellular/PageChooseApn.qml:109
msgid "Internet"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:138
#: ../plugins/cellular/PageChooseApn.qml:113
msgid "MMS"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:139
#: ../plugins/cellular/PageChooseApn.qml:111
msgid "LTE"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:157
msgid "Name"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:166
msgid "Enter a name describing the APN"
msgstr ""

#. TRANSLATORS: This string is a description of a text
#. field and should thus be concise.
#: ../plugins/cellular/PageApnEditor.qml:182
#: ../plugins/cellular/PageCarrierAndApn.qml:59
#: ../plugins/cellular/PageCarriersAndApns.qml:71
#: ../plugins/cellular/PageChooseApn.qml:41
msgid "APN"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:192
msgid "Enter the name of the access point"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:206
msgid "MMSC"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:216
msgid "Enter message center"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:234
msgid "Proxy"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:244
msgid "Enter message proxy"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:289
msgid "Proxy port"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:300
msgid "Enter message proxy port"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:318
msgid "User name"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:328
msgid "Enter username"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:341
msgid "Password"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:353
msgid "Enter password"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:367
msgid "Authentication"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:371
#: ../plugins/cellular/PageCarrierAndApn.qml:47
#: ../plugins/cellular/PageCarriersAndApns.qml:59
msgid "None"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:372
msgid "PAP or CHAP"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:373
msgid "PAP only"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:374
msgid "CHAP only"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:385
msgid "Protocol"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:389
msgid "IPv4"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:390
msgid "IPv6"
msgstr ""

#: ../plugins/cellular/PageApnEditor.qml:391
msgid "IPv4v6"
msgstr ""

#: ../plugins/cellular/PageCarrierAndApn.qml:27
msgid "Carrier & APN"
msgstr ""

#: ../plugins/cellular/PageCarrierAndApn.qml:45
#: ../plugins/cellular/PageCarrierAndApn.qml:54
#: ../plugins/cellular/PageCarriersAndApns.qml:56
#: ../plugins/cellular/PageChooseCarrier.qml:32
#: ../plugins/cellular/PageChooseCarrier.qml:129
msgid "Carrier"
msgstr ""

#: ../plugins/cellular/PageCarriersAndApns.qml:27
msgid "Carriers & APNs"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:85
msgid "MMS APN"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:91
msgid "Internet APN"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:97
msgid "LTE APN"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:156
msgid "Reset All APN Settings…"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:165
msgid "Reset APN Settings"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:166
msgid "Are you sure that you want to Reset APN Settings?"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:169
msgid "Reset"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:178
#: ../plugins/cellular/PageChooseApn.qml:209
#: ../plugins/cellular/PageChooseApn.qml:240
#: ../plugins/cellular/PageChooseApn.qml:274
#, fuzzy
msgid "Cancel"
msgstr "Cancel"

#. TRANSLATORS: %1 is the MMS APN that the user has chosen to be
#. “preferred”.
#. TRANSLATORS: %1 is the Internet APN that the user has chosen to
#. be “preferred”.
#. TRANSLATORS: %1 is the MMS APN that the user has chosen to be
#. “preferred”.
#. TRANSLATORS: %1 is the Internet APN that the user has chosen to
#. be “preferred”.
#. TRANSLATORS: %1 is the MMS APN that the user has chosen to be
#. “preferred”.
#. TRANSLATORS: %1 is the Internet APN that the user has chosen to
#. be “preferred”.
#: ../plugins/cellular/PageChooseApn.qml:192
#: ../plugins/cellular/PageChooseApn.qml:223
#, qt-format
msgid "Prefer %1"
msgstr ""

#. TRANSLATORS: %1 is the MMS APN that the user has chosen to be
#. “preferred”, i.e. used to retrieve MMS messages. %2 is the Internet
#. APN that will be “de-preferred” as a result of this action.
#: ../plugins/cellular/PageChooseApn.qml:196
#, qt-format
msgid "You have chosen %1 as your preferred MMS APN. "
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:201
#: ../plugins/cellular/PageChooseApn.qml:266
msgid "Disconnect"
msgstr ""

#. TRANSLATORS: %1 is the Internet APN that the user has chosen to
#. be “preferred”, i.e. used to connect to the Internet. %2 is the MMS
#. APN that will be “de-preferred” as a result of this action.
#: ../plugins/cellular/PageChooseApn.qml:227
#, qt-format
msgid "You have chosen %1 as your preferred Internet APN. "
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:232
#: ../plugins/cellular/PageChooseApn.qml:265
msgid "Disable"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:254
#, qt-format
msgid "Disconnect %1"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:255
#, qt-format
msgid "Disable %1"
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:260
#, qt-format
msgid "This disconnects %1."
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:261
#, qt-format
msgid "This disables %1."
msgstr ""

#: ../plugins/cellular/PageChooseApn.qml:303
#, fuzzy
msgid "Delete"
msgstr "Delete"

#: ../plugins/cellular/PageChooseCarrier.qml:153
msgid "Automatically"
msgstr ""

#: ../plugins/cellular/PageComponent.qml:32
msgid "Cellular"
msgstr ""
