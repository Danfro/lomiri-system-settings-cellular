# Italian translation for lomiri-system-settings-cellular
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the lomiri-system-settings-cellular package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-cellular\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-29 13:32+0000\n"
"PO-Revision-Date: 2023-04-02 03:46+0000\n"
"Last-Translator: Sylke Vicious <silkevicious@tuta.io>\n"
"Language-Team: Italian <https://hosted.weblate.org/projects/lomiri/"
"lomiri-system-settings-cellular/it/>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.17-dev\n"
"X-Launchpad-Export-Date: 2015-07-16 05:40+0000\n"

#: ../plugins/cellular/PageApnEditor.qml:71
msgid "Edit"
msgstr "Modifica"

#: ../plugins/cellular/PageApnEditor.qml:71
msgid "New APN"
msgstr "Nuovo APN"

#: ../plugins/cellular/PageApnEditor.qml:132
msgid "Used for:"
msgstr "Usato per:"

#: ../plugins/cellular/PageApnEditor.qml:136
#: ../plugins/cellular/PageChooseApn.qml:107
msgid "Internet and MMS"
msgstr "Internet ed MMS"

#: ../plugins/cellular/PageApnEditor.qml:137
#: ../plugins/cellular/PageChooseApn.qml:109
msgid "Internet"
msgstr "Internet"

#: ../plugins/cellular/PageApnEditor.qml:138
#: ../plugins/cellular/PageChooseApn.qml:113
msgid "MMS"
msgstr "MMS"

#: ../plugins/cellular/PageApnEditor.qml:139
#: ../plugins/cellular/PageChooseApn.qml:111
msgid "LTE"
msgstr "LTE"

#: ../plugins/cellular/PageApnEditor.qml:157
msgid "Name"
msgstr "Nome"

#: ../plugins/cellular/PageApnEditor.qml:166
msgid "Enter a name describing the APN"
msgstr "Inserisci un nome che descriva l'APN"

#. TRANSLATORS: This string is a description of a text
#. field and should thus be concise.
#: ../plugins/cellular/PageApnEditor.qml:182
#: ../plugins/cellular/PageCarrierAndApn.qml:59
#: ../plugins/cellular/PageCarriersAndApns.qml:71
#: ../plugins/cellular/PageChooseApn.qml:41
msgid "APN"
msgstr "Punti di accesso"

#: ../plugins/cellular/PageApnEditor.qml:192
msgid "Enter the name of the access point"
msgstr "Inserisci il nome dell'access point"

#: ../plugins/cellular/PageApnEditor.qml:206
msgid "MMSC"
msgstr "MMSC"

#: ../plugins/cellular/PageApnEditor.qml:216
msgid "Enter message center"
msgstr "Inserire il centro di messaggi"

#: ../plugins/cellular/PageApnEditor.qml:234
msgid "Proxy"
msgstr "Proxy"

#: ../plugins/cellular/PageApnEditor.qml:244
msgid "Enter message proxy"
msgstr "Inserisci proxy messaggio"

#: ../plugins/cellular/PageApnEditor.qml:289
msgid "Proxy port"
msgstr "Porta del proxy"

#: ../plugins/cellular/PageApnEditor.qml:300
msgid "Enter message proxy port"
msgstr "Inserisci la porta del proxy del messaggio"

#: ../plugins/cellular/PageApnEditor.qml:318
msgid "User name"
msgstr "Nome utente"

#: ../plugins/cellular/PageApnEditor.qml:328
msgid "Enter username"
msgstr "Inserire nome utente"

#: ../plugins/cellular/PageApnEditor.qml:341
msgid "Password"
msgstr "Password"

#: ../plugins/cellular/PageApnEditor.qml:353
msgid "Enter password"
msgstr "Inserire password"

#: ../plugins/cellular/PageApnEditor.qml:367
msgid "Authentication"
msgstr "Autenticazione"

#: ../plugins/cellular/PageApnEditor.qml:371
#: ../plugins/cellular/PageCarrierAndApn.qml:47
#: ../plugins/cellular/PageCarriersAndApns.qml:59
msgid "None"
msgstr "Nessuna"

#: ../plugins/cellular/PageApnEditor.qml:372
msgid "PAP or CHAP"
msgstr "PAP o CHAP"

#: ../plugins/cellular/PageApnEditor.qml:373
msgid "PAP only"
msgstr "Solo PAP"

#: ../plugins/cellular/PageApnEditor.qml:374
msgid "CHAP only"
msgstr "Solo CHAP"

#: ../plugins/cellular/PageApnEditor.qml:385
msgid "Protocol"
msgstr "Protocollo"

#: ../plugins/cellular/PageApnEditor.qml:389
msgid "IPv4"
msgstr "IPv4"

#: ../plugins/cellular/PageApnEditor.qml:390
msgid "IPv6"
msgstr "IPv6"

#: ../plugins/cellular/PageApnEditor.qml:391
msgid "IPv4v6"
msgstr "IPv4v6"

#: ../plugins/cellular/PageCarrierAndApn.qml:27
msgid "Carrier & APN"
msgstr "Operatore e APN"

#: ../plugins/cellular/PageCarrierAndApn.qml:45
#: ../plugins/cellular/PageCarrierAndApn.qml:54
#: ../plugins/cellular/PageCarriersAndApns.qml:56
#: ../plugins/cellular/PageChooseCarrier.qml:32
#: ../plugins/cellular/PageChooseCarrier.qml:129
msgid "Carrier"
msgstr "Operatore"

#: ../plugins/cellular/PageCarriersAndApns.qml:27
msgid "Carriers & APNs"
msgstr "Operatori e APN"

#: ../plugins/cellular/PageChooseApn.qml:85
msgid "MMS APN"
msgstr "Punti di accesso MMS"

#: ../plugins/cellular/PageChooseApn.qml:91
msgid "Internet APN"
msgstr "Punti di accesso Internet"

#: ../plugins/cellular/PageChooseApn.qml:97
msgid "LTE APN"
msgstr "Punto di accesso LTE"

#: ../plugins/cellular/PageChooseApn.qml:156
msgid "Reset All APN Settings…"
msgstr "Resetta le impostazioni dei punti di acesso…"

#: ../plugins/cellular/PageChooseApn.qml:165
msgid "Reset APN Settings"
msgstr "Ripristina punti di accesso"

#: ../plugins/cellular/PageChooseApn.qml:166
msgid "Are you sure that you want to Reset APN Settings?"
msgstr "Sicuro di voler fare il reset della configurazione dell'APN?"

#: ../plugins/cellular/PageChooseApn.qml:169
msgid "Reset"
msgstr "Resetta"

#: ../plugins/cellular/PageChooseApn.qml:178
#: ../plugins/cellular/PageChooseApn.qml:209
#: ../plugins/cellular/PageChooseApn.qml:240
#: ../plugins/cellular/PageChooseApn.qml:274
msgid "Cancel"
msgstr "Annulla"

#. TRANSLATORS: %1 is the MMS APN that the user has chosen to be
#. “preferred”.
#. TRANSLATORS: %1 is the Internet APN that the user has chosen to
#. be “preferred”.
#. TRANSLATORS: %1 is the MMS APN that the user has chosen to be
#. “preferred”.
#. TRANSLATORS: %1 is the Internet APN that the user has chosen to
#. be “preferred”.
#. TRANSLATORS: %1 is the MMS APN that the user has chosen to be
#. “preferred”.
#. TRANSLATORS: %1 is the Internet APN that the user has chosen to
#. be “preferred”.
#: ../plugins/cellular/PageChooseApn.qml:192
#: ../plugins/cellular/PageChooseApn.qml:223
#, qt-format
msgid "Prefer %1"
msgstr "Preferisci %1"

#. TRANSLATORS: %1 is the MMS APN that the user has chosen to be
#. “preferred”, i.e. used to retrieve MMS messages. %2 is the Internet
#. APN that will be “de-preferred” as a result of this action.
#: ../plugins/cellular/PageChooseApn.qml:196
#, qt-format
msgid "You have chosen %1 as your preferred MMS APN. "
msgstr "Hai scelto %1 come APN preferito per gli MMS. "

#: ../plugins/cellular/PageChooseApn.qml:201
#: ../plugins/cellular/PageChooseApn.qml:266
msgid "Disconnect"
msgstr "Disconnetti"

#. TRANSLATORS: %1 is the Internet APN that the user has chosen to
#. be “preferred”, i.e. used to connect to the Internet. %2 is the MMS
#. APN that will be “de-preferred” as a result of this action.
#: ../plugins/cellular/PageChooseApn.qml:227
#, qt-format
msgid "You have chosen %1 as your preferred Internet APN. "
msgstr "Hai scelto %1 come APN preferito per Internet. "

#: ../plugins/cellular/PageChooseApn.qml:232
#: ../plugins/cellular/PageChooseApn.qml:265
msgid "Disable"
msgstr "Disabilita"

#: ../plugins/cellular/PageChooseApn.qml:254
#, qt-format
msgid "Disconnect %1"
msgstr "Disconnetti %1"

#: ../plugins/cellular/PageChooseApn.qml:255
#, qt-format
msgid "Disable %1"
msgstr "Disattiva %1"

#: ../plugins/cellular/PageChooseApn.qml:260
#, qt-format
msgid "This disconnects %1."
msgstr "Questo disconnette %1."

#: ../plugins/cellular/PageChooseApn.qml:261
#, qt-format
msgid "This disables %1."
msgstr "Questo disabilita %1."

#: ../plugins/cellular/PageChooseApn.qml:303
msgid "Delete"
msgstr "Elimina"

#: ../plugins/cellular/PageChooseCarrier.qml:153
msgid "Automatically"
msgstr "Automaticamente"

#: ../plugins/cellular/PageComponent.qml:32
msgid "Cellular"
msgstr "Rete mobile"

#~ msgid "OK"
#~ msgstr "OK"

#~ msgid "Cellular data"
#~ msgstr "Dati mobili"

#~ msgid "No SIM detected"
#~ msgstr "Nessuna SIM rilevata"

#~ msgid "Insert a SIM, then restart the device."
#~ msgstr "Inserire una SIM, quindi riavviare il dispositivo."

#~ msgid "Data roaming"
#~ msgstr "Roaming dati"

#~ msgid "Ask me each time"
#~ msgstr "Chiedi ogni volta"

#~ msgid "For outgoing calls, use:"
#~ msgstr "Per le chiamate in uscita, usa:"

#~ msgid ""
#~ "You can change the SIM for individual calls, or for contacts in the "
#~ "address book."
#~ msgstr ""
#~ "È possibile modificare la SIM da usare per ogni chiamata o per ogni "
#~ "contatto in rubrica."

#~ msgid "For messages, use:"
#~ msgstr "Per i messaggi, usa:"

#~ msgid "Next"
#~ msgstr "Avanti"

#~ msgid "Data usage statistics"
#~ msgstr "Statistiche di utilizzo dei dati"

#~ msgid "Carriers"
#~ msgstr "Operatori"

#~ msgid "Connection type:"
#~ msgstr "Tipo di connessione:"

#~ msgid "2G only (saves battery)"
#~ msgstr "Solo 2G (risparmia batteria)"

#~ msgid "2G/3G (faster)"
#~ msgstr "2G/3G (più veloce)"

#~ msgid "2G/3G/4G (faster)"
#~ msgstr "2G/3G/4G (più veloce)"

#~ msgid "Edit SIM Name"
#~ msgstr "Modifica nome SIM"

#~ msgid "Cellular data:"
#~ msgstr "Dati mobili:"

#~ msgid "cellular"
#~ msgstr "cellulare"

#~ msgid "network"
#~ msgstr "rete"

#~ msgid "mobile"
#~ msgstr "mobile"

#~ msgid "gsm"
#~ msgstr "gsm"

#~ msgid "data"
#~ msgstr "dati"

#~ msgid "carrier"
#~ msgstr "operatore"

#~ msgid "4g"
#~ msgstr "4g"

#~ msgid "3g"
#~ msgstr "3g"

#~ msgid "2g"
#~ msgstr "2g"

#~ msgid "lte"
#~ msgstr "LTE"

#~ msgid "apn"
#~ msgstr "APN"

#~ msgid "roam"
#~ msgstr "roam"

#~ msgid "sim"
#~ msgstr "sim"
